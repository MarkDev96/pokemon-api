# JPW - 2021 UNESC
Disciplina de Java para Web
Professor Ramon Venson

[API URL](https://ppw-pokemon-api.herokuapp.com)

Crie um fork do repositório implemente as alterações nas funções dapasta API. Proponha um merge request para as funções implementadas.

* api/aleatorio.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/aleatorio))
* api/all.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/all))
* api/balanceados.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/balanceados))
* api/fragil.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/fragil))
* api/johto_pokedex.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/johto_pokedex))
* api/kanto_pokedex.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/kanto_pokedex))
* api/lentos.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/lentos))
* api/letras.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/letras))
* api/rapidos.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/rapidos))
* api/soma_ataques_sp.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/soma_ataques_sp))
* api/soma_ataques.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/soma_ataques))
* api/soma_defesas_sp.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/soma_defesas_sp))
* api/soma_defesas.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/soma_defesas))
* api/sprites.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/sprites))
* api/tanks.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/tanks))
* api/tipo_agua.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/tipo_agua))
* api/tipo_duplo.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/tipo_duplo))
* api/tipo_fogo.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/tipo_fogo))
* api/tipo_grama.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/tipo_grama))
* api/ubers.js ([endpoint](https://ppw-pokemon-api.herokuapp.com/api/ubers))

## Modelo de Dados

Todos os endpoints já exportam por padrão o arquivo `data.json`, que contém uma lista de pokémons com seus respectivos atributos. Você pode acessar essa lista dentro do arquivo de cada uma das rotas através da variável `data`. O modelo de dados usado em cada objeto da lista segue o seguinte esquema:

````js
{
    "national_number" : String, // ex.: "001"
    "evolution": Array,
    "sprites": {
        "normal": String,  // ex.: "https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/bulbasaur.png"
        "large": String, // ex.: "https://img.pokemondb.net/artwork/bulbasaur.jpg"
        "animated": String, // ex.: "https://img.pokemondb.net/sprites/black-white/anim/normal/bulbasaur.gif"
    },
    "name": String, // ex.: "Bulbasaur",
    "type": [
        String // ex.: Grass
    ],
    "total": Number, // ex.: 318
    "hp": Number, // ex.: 45
    "attack": Number, // ex.: 49
    "defense": Number, // ex.: 49
    "sp_atk": Number, // ex.: 65
    "sp_def": Number, // ex.: 65
    "speed": Number, // ex.: 45
}
````