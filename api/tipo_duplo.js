/*
    Lista apenas os pokemons com dois tipos diferentes (type)
*/

const data = require('../data')

module.exports = function(req, res){

    let result = data.filter((pokemon) => pokemon.type.length == 2)
    res.json(result)
}

