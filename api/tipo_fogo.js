/*
    Lista apenas os pokemons do tipo fogo (type = fire)
*/

const data = require('../data')

module.exports = function(req, res) {
    const response = data.filter((pokemon) => pokemon.type.indexOf("Fire") !== -1)
    res.json(response)
}